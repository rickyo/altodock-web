<?php



/*contact_us */
$contact_subject = "閣下的查詢已發送"; 
$contact_content =<<<CONTENT

<p>親愛的___NAME___您好 </p>		

<p><center><span style="font-size:11pt; font-weight:bold">聯絡我們</span></center></p>

<p>多謝您對「___SUBJECT___」的查詢，我們將會盡快與閣下聯絡。</p>
<p>如有任何疑問，可經以下途徑聯絡我們:</p>
<p>電郵: __COMPANY_EMAIL__</p>
<div style="margin-top:20px;">
<p>您的信息如下 :</p>
<p><span style="width :200px; float:left; margin-right:10px;"> 姓名: </span><span>___NAME___</span></p>
<p><span style="width :200px; float:left; margin-right:10px;"> 電郵: </span><span>___EMAIL___</span></p>
<p><span style="width :200px; float:left; margin-right:10px;"> 查詢: </span><span>___SUBJECT___</span></p>
<p><span style="width :200px; float:left; margin-right:10px;"> 內容: </p>

<p style="padding : 20px; min-height : 100px; ">___MSG___</p>
</div>	

<br/><br/>
Coolocat Limited<br />
__COMPANY_EMAIL__<br />
********************************************************<br/>
<br/>
請勿回覆此郵件。傳送至此地址的郵件不能得到回應。 <br/>

CONTENT;


?>