<?php
set_time_limit(30);

// Configuration
if (file_exists('config.php')) {
	require_once('config.php');
}

// separate sessions between production & deploy
if ( stripos($_SERVER['REQUEST_URI'], '/'._HOME.'/') !== false ): // main site
    $session_name = 'session_'._HOME;
	$cookie_dir = _HOME;
else:
    $session_name = 'session_'._HOME.'_deploy_uat';	//deploy
	$cookie_dir = _HOME."_deploy_uat";
endif;
/*
// Hotfix
if (!defined('_DEPLOY')){
	if(preg_match('/^\/('._HOME.'|en)$/', $_SERVER['REQUEST_URI']) ){
		header("Location:".$_SERVER['REQUEST_URI']."/");
	}
}

if( $_SERVER['REQUEST_URI'] =="/" ){
	header("Location:/"._HOME."/");
}*/

// ignore users
if( isset( $_SERVER['HTTP_USER_AGENT'] ) ) {
    if(
   	 strstr($_SERVER['HTTP_USER_AGENT'], "http://www.baidu.com/search/spider.html") ||
   	 strstr($_SERVER['HTTP_USER_AGENT'], "http://www.matuschek.net/jobo.html") ||
   	 strstr($_SERVER['HTTP_USER_AGENT'], "Xaldon WebSpider") ||
   	 strstr($_SERVER['HTTP_USER_AGENT'], "www.sogou.com") ||
   	 strstr($_SERVER['HTTP_USER_AGENT'], "+http://www.trovit.com/bot.html") ||
   	 strstr($_SERVER['HTTP_USER_AGENT'], "http://help.soso.com/webspider.htm")
    ) {
   	 error_log("[".date("Y-m-d H:i:s")."] ".$_SERVER['REMOTE_ADDR']." Page exit - ".$_SERVER['HTTP_USER_AGENT']." \n", 3, "/tmp/www_exit_".date("Y-m-d").".log");
   	 exit;
    }
}

// Startup
require_once(DIR_SYSTEM . '/startup.php');
//require_once(DIR_SYSTEM . '/database/database.php');
//require_once(DIR_SYSTEM . '/database/mysqldb.php');
/*if (!defined('_LOCALHOST')){
	require_once(DIR_SYSTEM . '/database/mysqldb.php');
}else{
	require_once(DIR_SYSTEM . '/database/database.php');
}*/
//require_once(DIR_SYSTEM . '/database/connection.php');


// Class
//include_once(DIR_CLASS."/WebsiteUtility.class.php");
//include_once CCCORE."/Common/includes/Utility.php";

ob_start();

/*
//common
if (isset($_POST["config_lang"]) && $_POST["config_lang"] != ""){
	$config_lang = $_POST["config_lang"];
	$_SESSION["lang"] = $config_lang;
}else{
	
}*/

	$config_lang = "en";
	$url_parts = parse_url($_SERVER["REQUEST_URI"]);

	if (stripos($url_parts["path"], "/zh/") !== false){
		$config_lang = "zh";
	}
	$_SESSION["lang"] = $config_lang;
	



// Registry
$registry = new Registry();

// Loader
$loader = new Loader($registry);
$registry->set('load', $loader);

// Config
$config = new Config();
$config->set('config_language_id', 1);
$registry->set('config', $config);

//Database
/*
$db = myDB::db_database_connect(DBNAME,DBHOST,DBUSERNAME,DBPASSWORD,MYSQLPCONNECT) or die('Unable to connect to database server!');
ini_set('session.bug_compat_42', 1);
ini_set('session.bug_compat_warn', 0);
$registry->set('db', $db);
*/

// Session
$session = new Session($session_name, $cookie_dir, _SITE);
$registry->set($session_name, $session);
$registry->set('session', $session_name);


// lang (before session)
$registry->set('lang',$config_lang);


$_SESSION["lang"] =$config_lang;


//var_dump($_SESSION, $_POST, $config_lang);

require_once DIR_LANGUAGE."/common.lang.php";

// Language Detection
$languages = array();
$registry->set('__', $__);	//lang
$__ = $registry->get("__");

// Error Handler
//set_error_handler('error_handler');

// Request
$request = new Request();
$registry->set('request', $request);

// Response
$response = new Response();
$response->addHeader('Content-Type: text/html; charset=utf-8');
$response->setCompression($config->get('config_compression'));
$registry->set('response', $response);

// Cache
$cache = new Cache();
$registry->set('cache', $cache);

if (isset($request->get['tracking'])) {
	setcookie('tracking', $request->get['tracking'], time() + 3600 * 24 * 1000, '/');
}

// Page
//$page = new CPage();

require_once("./router.php");






ob_end_flush();


?>