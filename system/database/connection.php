<?php
// connections : database functions 



function getQuery($query)
{
	if (isset($query) && trim($query) != ""){
		 $db_result = myDB::db_query($query);
		 $result = myDB::getRecordArray($db_result);
			
		 if (strpos(strtoupper($query), 'SELECT')===false)	
			return $db_result;
		 else		
			return $result; // select - load data
	}
		 
	return false;	 
}

function getInsertID()
{
	return myDB::db_insert_id();
}

function getRecord($table_name,$select_field_name="",$parameter_list="",$order_field_list="",$start=0,$nrecords=1000,$totalcount=true)
{
    //echo $table_name;
	
	if(trim($select_field_name)!==""){

		$sql_data_array=array($select_field_name => $select_field_name);

	}
	
	$param = array();
	
	if(trim($parameter_list)<>"")

		$param["param"] = $parameter_list;



	if(trim($order_field_list)<>"")

		$param["order"] = $order_field_list;



	if(!$totalcount)

		$param["limit"] =" LIMIT ".(int)$start.", ".(int)$nrecords ;




	if(($rst=myDB::db_select($table_name,$sql_data_array,$param))){

		$rstArray=myDB::getRecordArray($rst);

		return $rstArray;

	}else

		return false;

	

}


?>