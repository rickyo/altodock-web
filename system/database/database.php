
<?php 
//	=====================================================

//	start class myBuildQuery

//	=====================================================

class myBuildQuery 
{


	static public $dbcnx;

	static public function db_data_prepare($string)
	{
		if(is_string($string))
		{
			return addslashes(stripslashes(trim($string)));

		}


	}
	
	static public function db_database_connect($dbname,$dbhost,$dbusername,$dbpassword)
	{

		
		$dbcnx = mysqli_connect($dbhost,$dbusername,$dbpassword); // connect to the database

		

		if($dbcnx){
			mysqli_select_db($dbcnx, $dbname);
			mysqli_set_charset($dbcnx, "utf8");
		}
		
		self::$dbcnx = $dbcnx;


		return $dbcnx;

	}
	

	static public function db_insert($table,$data)
	{

		reset($data);

		$str_sql="insert into ".$table."(";

		while((list($columns,)=each($data)))

		{

			if(!isset($str_sql1))

			{

				$str_sql1=$columns;

			}

			else

			{

				$str_sql1.=','.$columns;

			}

		}

		$str_sql.=$str_sql1.") values(";

		reset($data);


		while((list(,$values)=each($data)))

		{

			switch((string)$values)

			{

				case 'now()':

					$query.='now(),';

					break;

				default:

					$query.='\''.$values.'\',';

					break;

			}

			

		}

		$query=substr($query,0,-1);

		$str_sql.=$query.")";

	  // echo  $str_sql;
	 // echo  $str_sql."<br>";
		return $str_sql;

	}

	static public function db_update($table,$data,$parameter='')
	{

		reset($data);


		$conditions = "";
		
		while((list($column,$values)=each($data)))

		{

			switch((string)$values)

			{

				case 'now()':

					$conditions.='now(),';

					break;

				default:

					$conditions.=$column.'=\''.$values.'\',';

					break;

			}

		}

		$conditions=substr($conditions,0,-1);	

		 $query="update ".$table." set ".$conditions;

		if(!empty($parameter))

		{

			$query.=" where ".$parameter;

		}
		//echo  $query; 
		return $query;

	}

	static public function db_select($table,$data,$parameter)

	{

		reset($data);
		$conditions = "";
		while((list(,$values)=each($data)))

		{

			switch((string)$values)

			{

				case 'now()':

					$conditions.='now()';

					break;

				default:

					$conditions.=$values.',';

					break;

			}

		}

		$conditions=substr($conditions,0,-1);

		$query="select ".$conditions." from ".$table;

		if(isset($parameter["param"]) && trim($parameter["param"]) != "")
		{
		
			$query.=" where ".$parameter["param"];

		}
		
		if(isset($parameter["order"]) && trim($parameter["order"]) != "")
		{
		
			$query.=" order by ".$parameter["order"];

		}
		
		if( isset($parameter["limit"]) && trim($parameter["limit"]) != "")
		{
		
			$query.= $parameter["limit"];

		}
		
		return $query;
	}
	
		// database functions
	static public function getRecordArray($rst)
	{

		@mysqli_data_seek($rst);

		$record=@array();

		$noOfRows=@mysqli_num_rows($rst);

		$noOfColumns=@mysqli_num_fields($rst);

		$col=array($noOfColumns);

		for($r=0;$r<$noOfRows;$r++){
			
			$record[$r]=@mysqli_fetch_assoc($rst);

		}

		return $record;

	}

}

//	=====================================================

//	end class myBuildQuery

//	=====================================================
//	=====================================================

//	start class myDB

//	=====================================================

class myDB extends myBuildQuery 
{

	static public function db_show_query($query) { 

		if(DEBUG)echo('<br><br><small><font color="#ff0000">-- Database Query --</font></small></br><font color="#000000" size="2"><b>' . $query . '<br><br><small><font color="#ff0000">[STOP]</font></small><br><br></b></font>');

	}

	static public function db_error($query, $errno, $error) { 

		die('<font color="#000000" size="2"><b>' . $errno . ' - ' . $error . '<br><br>' . $query . '<br><br><small><font color="#ff0000">[STOP]</font></small><br><br></b></font>');

	}
	

	static public function db_add($table,$data_array)
	{

		$query = parent :: db_insert($table,$data_array);

		self::db_show_query($query);

		$result = mysqli_query(self :: $dbcnx, $query) or (self::db_error($query, mysqli_errno(), mysqli_error()));
    //echo $result; 
		return $result;

	}


 	static public function db_update($table,$data_array,$param='')
	{

		$query = parent :: db_update($table,$data_array,$param);

		self::db_show_query($query);

		$result = mysqli_query(self :: $dbcnx, $query) or (self::db_error($query, mysqli_errno(), mysqli_error()));

		return $result;

	} 

	static public function db_query($query)
	{


		self::db_show_query($query);


		$result = mysqli_query(self::$dbcnx, $query) or (self::db_error($query, mysqli_errno(), mysqli_error()));

		return $result;

	}
	

	static public function db_select($table,$data_array,$param='')
	{

	    $query = parent :: db_select($table,$data_array,$param);


		self::db_show_query($query);


		$result = mysqli_query(self::$dbcnx, $query) or (self::db_error($query, mysqli_errno(), mysqli_error()));

		return $result;

	}
 
 
	static public function db_insert_id()
	{
		return mysqli_insert_id(self::$dbcnx);

	}

}	
//	=====================================================

//	end class myDB

//	=====================================================


?>