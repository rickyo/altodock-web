<?php
class Session {
	public $data = array();
			
  	public function __construct($session_name = "", $cookie_dir = "", $site = "") {		
		if (!session_id()) {
			ini_set('session.use_cookies', 'On');
			ini_set('session.use_trans_sid', 'Off');
			//ini_set('session.save_path', '/'.$cookie_dir);

			session_name($session_name);
			session_set_cookie_params(0, '/');//, $site , false, true);
			session_start();
		
		}
			
		$this->data =& $_SESSION;
	}
	
	function getId() {
		return session_id();
	}
}
?>
