<?php

// Router
require DIR_SYSTEM.'/Router.php';
require DIR_SYSTEM.'/Route.php';


$router = new Router();

if (!defined('_LOCALHOST')){
	$router->setBasePath("");//._LOCALE);
}else{	
	$router->setBasePath("/".trim(_LOCALE, "/"));
}

$router->map('/', array("page_type"=>"html", "nav"=>"home.php")); // first page
$router->map('/zh/', array("page_type"=>"html", "nav"=>"home.php"));
$router->map('/:lang', array("page_type"=>"html", "nav"=>"home.php"),array('filters' => array('lang' => '(\w+)'))); // first page
$router->map('/:lang/home', array("page_type"=>"html", "nav"=>"home.php"),array('filters' => array('lang' => '(\w+)')));
$router->map('/:lang/tvp', array("page_type"=>"html", "nav"=>"tvp.php"),array('filters' => array('lang' => '(\w+)')));
$router->map('/:lang/trading', array("page_type"=>"html", "nav"=>"trading.php"),array('filters' => array('lang' => '(\w+)')));
$router->map('/:lang/ecommerce', array("page_type"=>"html", "nav"=>"ecommerce.php"),array('filters' => array('lang' => '(\w+)')));
$router->map('/:lang/services', array("page_type"=>"html", "nav"=>"services.php"),array('filters' => array('lang' => '(\w+)')));
$router->map('/:lang/services/:page', array("page_type"=>"html", "nav"=>"services.php"),array('filters' => array('page' => '(\w+)', 'lang' => '(\w+)')));

$router->map('/:lang/about_us', array("page_type"=>"html", "nav"=>"about_us.php"),array('filters' => array('lang' => '(\w+)')));
$router->map('/:lang/contact_us', array("page_type"=>"html", "nav"=>"contact_us.php"),array('filters' => array('lang' => '(\w+)')));

$router->map('/:lang/disclaimer', array("page_type"=>"html", "nav"=>"disclaimer.php"),array('filters' => array('lang' => '(\w+)')));
$router->map('/:lang/terms-conditions', array("page_type"=>"html", "nav"=>"terms_condition.php"),array('filters' => array('lang' => '(\w+)')));
$router->map('/:lang/privacy-statement', array("page_type"=>"html", "nav"=>"privacy_policy.php"),array('filters' => array('lang' => '(\w+)')));


$router->map('/profile/:user_id', array("page_type"=>"html", "nav"=>"cardprofile.php"),array('filters' => array('user_id' => '(\w+)')));


$router->map('/:lang/page_error', array("page_type"=>"html", "nav"=>"page_not_found.php"),array('filters' => array('lang' => '(\w+)')));
$router->map('/:lang/register', array("page_type"=>"html", "nav"=>"register.php"),array('filters' => array('lang' => '(\w+)')));
$router->map('/:lang/thankyou', array("page_type"=>"html", "nav"=>"thankyou.php"),array('filters' => array('lang' => '(\w+)')));
$router->map('/:lang/sendmail', array("page_type"=>"html", "nav"=>"sendmail.php"),array('filters' => array('lang' => '(\w+)')));
$router->map('/:lang/regmail', array("page_type"=>"html", "nav"=>"rd-mailform.php"),array('filters' => array('lang' => '(\w+)')));

$router->map('/:lang/testmail', array("page_type"=>"html", "nav"=>"bat/emailtest.php"),array('filters' => array('lang' => '(\w+)')));


$route = $router->matchCurrentRequest();

//var_dump($route);

if ($route){

	$target = $route->getTarget();
	$param = $route->getParameters();


	switch ($target["page_type"]){

		case "html":
			require (DIR_APPLICATION. "/".$target["nav"]);
			break;

		case "admin_html":
			require (DIR_APPLICATION_ADMIN . "/".$target["nav"]);
			break;

		case "api":
			require (DIR_API."/". $target["nav"]);
			break;

		case "lib":
			require (DIR_LIB. "/".$target["nav"]);
			break;

		case "class":
			require (DIR_CLASS. "/".$target["nav"]);
			break;

		default:
			require ($target["page_type"]. "/".$target["nav"]);
			break;

	}
//	$action = new Action('common/home');
}else{
	require (DIR_APPLICATION.'/page_not_found.php');
}

?>