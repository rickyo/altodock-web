<?php
//define('_LOCALHOST', 1);

//define('_DEPLOY',1);

// Version
define('VERSION', '1.0.1');

if (!defined('_LOCALHOST')){

	$locale="en";
	define('_LOCALE', $locale);	
	define('_SITE', "www.altodock.com");
	define('_HOME',  $locale);	
	define("SITE_DOMAIN", "www.altodock.com");	//veeko changed to hardcode 26Jan20
	

}else{

	$locale = "altodock.en";
	define('_LOCALE',  $locale);	
	define('_SITE', "127.0.0.1");
	define('_HOME',  $locale);	
	define("SITE_DOMAIN", $_SERVER["SERVER_NAME"]);
	
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(E_ALL);
}

define("_HOMEURL", _SITE);


date_default_timezone_set('Asia/Hong_Kong');


// constants
$site_dir =  $_SERVER["DOCUMENT_ROOT"];//realpath(dirname(__FILE__)."/");

//define("SITE_DOMAIN", $_SERVER["SERVER_NAME"]);
//define("SITE_DOMAIN", "www.altodock.com");	//veeko changed to hardcode 26Jan20
define("SITE_URL", "https://".SITE_DOMAIN);
define("SITE_PATH", $site_dir."/"._LOCALE);
define("SITE_DIR", "");
define("EMAIL_PATH", SITE_PATH."/email");
define("TPL_PATH", SITE_PATH."/view");
define("CLASS_PATH", SITE_PATH."/class");
define("LIB_PATH", SITE_PATH."/lib");
define("SYS_PATH", SITE_PATH."/system");

// HTTP
define('HTTP_SERVER', SITE_URL."/"._LOCALE);

// HTTPS
define('HTTPS_SERVER', 'https://'._HOMEURL."/"._LOCALE);

// DIR

define('DIR_APPLICATION', SITE_PATH.'/application');
define('DIR_API', SITE_PATH.'/api');
define('DIR_SYSTEM', SITE_PATH.'/system');
//define('DIR_DATABASE', SITE_PATH.'/system/database');
define('DIR_LANGUAGE', SITE_PATH.'/language');
define('DIR_TEMPLATE', SITE_PATH.'/template');
define('DIR_CONFIG', SITE_PATH.'/system/config');
define('DIR_IMAGE', SITE_PATH.'/images');
define('DIR_CACHE', SITE_PATH.'/system/cache');
define('DIR_DOWNLOAD', SITE_PATH.'/download');
define('DIR_LOGS', SITE_PATH.'/system/logs');
define('DIR_INCLUDES', SITE_PATH.'/includes');
define('DIR_LIB', 	SITE_PATH.'/library');
define('DIR_CLASS', SITE_PATH.'/class');

define("JS_DIR", HTTP_SERVER."/assets/js");
define("CSS_DIR", HTTP_SERVER."/assets/css");
define("FONTS_DIR", HTTP_SERVER."/assets/fonts");
define("IMG_DIR", HTTP_SERVER."/images");
define("PLUGIN_DIR", HTTP_SERVER."/assets/plugin");
	

define('DIR_UPLOAD', $site_dir.'/'.'upload'); 
define('CCCORE', $site_dir.'/CCCore');


?>