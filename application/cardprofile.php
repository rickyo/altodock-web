<?php


// check any user id exists, redirecting home if empty 
if (!isset($param["user_id"])){
	header("Location: home.php");
}

// list of card user (hardcoded profile)
$card_user = array(
	"veekolam" => array("name" => "Veeko Lam", "tel" => "+852 5191 2505", "email"=>"veeko.lam@altodock.com", "ctitle"=>"Sr. Business Solution Consultant","profile_pic"=>"veeko.jpeg", "whatsapp"=>"https://wa.me/85251912505"),
	"lucaswong" => array("name" => "Lucas Wong", "tel" => "+852 6153 8382", "email"=>"lucas.wong@altodock.com", "ctitle"=>"Business Development Manager","profile_pic"=>"lucas.jpg?v=2", "whatsapp"=>"https://wa.me/85261538382", "wechatid"=>"lucaswong608", "ig"=>"https://www.instagram.com/lucazarts/", "about"=>"Founder of Altodock, Plantcake and Co-Founder of Simple House (Travel Kingdom)"),
	"rickychow" => array("name" => "Ricky Chow", "tel" => "+852 9615 2179", "email"=>"ricky.chow@altodock.com", "ctitle"=>"Senior IT Consultant","profile_pic"=>"ricky.jpg", "fb"=>"https://m.facebook.com/Rickyokita", "whatsapp"=>"https://wa.me/85296152179", "linkedin"=>"https://www.linkedin.com/in/ricky-chow-19305944/"),
	"benyiu" => array("name" => "Ben Yiu", "tel" => "+852 6469 7890", "email"=>"ben.yiu@altodock.com", "ctitle"=>"Business Development Manager", "profile_pic"=>"benyiu.jpg", "whatsapp"=>"https://wa.me/85264697890")

);


// searching user profile 
$userprofile = array();
foreach ($card_user as $userid => $user){ 

	if (strtolower($param["user_id"]) == strtolower(md5($userid))){
		$userprofile = $user;
		break;
	}

}

// check any valid user id, redirecting home if no profile found
if (sizeof($userprofile) <= 0){
	header("Location: home.php");
}



?>

<!DOCTYPE html>
<html lang="en">
<?php 
$css_list = array(
				"https://www.altodock.com/en/assets/css/profile/bootstrap.full.css",
				"https://www.altodock.com/en/assets/css/profile/base.css",
				"https://www.altodock.com/en/assets/css/profile/dashboard.css?v=2"
			);
?>
<?php require_once("global_header.php"); ?>
<style>
.js-loader-wrap{display:none;transition: all 2s linear;
    opacity: 0; }
.contacts{padding-top:6px;}
.contacts ul li{ list-style-type: none; }
.sep{
	padding-top:10px;
	border-top: 2px solid #ff8700;
	margin-bottom:10px;
}
.profileabout{
	
    color: #fff;
    padding-bottom: 10px;
    padding-left: 16px;


}

</style>

<body>
<div class="d-all">
			<div class="d-topbar">
		<div class="d-topbar-inner">
			<div class="d-container clearfix container-fluid">
				<div class="row">
					
					<div class="col-md-4 col-sm-4 col-xs-6 text-right d-topbar__profile-links">
						
							
							
							
							<div class="d-user ">
								
								<a href="#profile" class="d-user__link">
								
									<span class="d-user__link__content">
										
										<img src="/en/assets/images/profile/<?php echo $userprofile["profile_pic"]; ?>" alt="" class="d-user__avatar">
										
										<span class="d-user__name"><?php echo $userprofile["name"]; ?></span>
										
									</span>
									
								
								</a>
								
								
							</div>

							<div class="d-mobile-menu visible-xs">
									<span class="d-mobile-menu__burger icon-content-widget-mainmenu ul-icon js-ul-mobile-menu-dashboard"></span>

									<ul class="d-mobile-menu__content">
										<li class="d-mobile-menu__item">
											<a href="https://www.altodock.com" class="d-mobile-menu__link">
												<span class="icon-content-special-sites d-mobile-menu__icon"></span>
												<span class="d-mobile-menu__title">
														Home
												</span>
											</a>
										</li>
										
											<li class="d-mobile-menu__item">
													<a href="https://www.altodock.com/sites/url/jxfx3dv44/dashBoard/profile" class="d-user__link d-mobile-menu__link">
														<span class="d-user__link__content icon-content-special-user d-mobile-menu__icon"></span>
														<span class="d-mobile-menu__title">
															Profile
														</span>
													</a>
											</li>
										
									
									</ul>
							</div>
						
					</div>
					
				</div>
			</div>
		</div>
	</div>	
			
	<div class="d-topbar2">
		<div class="d-topbar2-inner">
			


<div class="d-container d-navbar container-fluid">
	<?php /* <div class="row sep">
		
		<div class="col-md-10 col-sm-10 col-xs-12">
			<h1 class="d-navbar__title"><?php echo $userprofile["name"]; ?></h1>
		</div>
	</div>*/ ?>
	<div class="row sep">
		
		<div class="col-md-10 col-sm-10 col-xs-12">
			<h1 class="d-navbar__title"><?php echo $userprofile["ctitle"]; ?></h1>
			
		</div>
	</div>
	<?php if (isset($userprofile["about"])){ ?>
		<div class="row profileabout">
		<?php echo $userprofile["about"]; ?>
		</div>
	<?php } ?>
	<div class="row contacts">
	
	  <ul>
		<li>Tel : <?php echo $userprofile["tel"]; ?></li>
		<li>Email : <?php echo $userprofile["email"]; ?></li>
		<?php if (isset($userprofile["wechatid"])){ ?>
			<li>Wechat ID : <?php echo $userprofile["wechatid"]; ?></li>
		<?php } ?>
	  </ul>
	</div>
</div>

		</div>
	</div>
	<div id="js-main" class="d-content UL UL-lightBG" role="main" data-site="jxfx3dv44">
		<div class="d-container cleafix">
			<input type="hidden" name="token" value="USyU6bD41upvYWJw1xEC" id="csrf-token">
			<div class="d-settings-leftmenu  d-settings-tabs-slider js-tabs-slider">
				<ul class="d-settings-tabs js-tabs js-slider-tabs d-tabs--events-added">
					
					<li class="d-settings-tab-wrap js-tab-wrap"><a href="#settings-profile-social" class="js-tab d-tab d-tab--active">Contact</a></li>
				
				</ul>

				<div class="d-settings-controls">
					<div class="d-settings-set-prev js-tab-control js-tab-prev disabled" data-direction="prev"></div>
					<div class="d-settings-set-next js-tab-control js-tab-next disabled" data-direction="next"></div>
				</div>
			</div>
			
			<div id="js-d-settings-user" class="d-settings-content d-settings-content--user">
				<div id="settings-profile-social" class="d-tabs-content d-tabs-content--visible">
					<div class="d-settings__block">
						<div class="d-settings__block__title">Social network accounts</div>
						<div class="d-settings__block__content">
							<div class="d-settings-socials">
								<ul>
									<?php 
									if (isset($userprofile["fb"])){ ?>
									<li class="d-settings-socials__item js-d-social-item " data-social="facebook">
										<span class="d-settings-socials__item__icon d-settings-socials__item__icon--facebook">
											<img src="https://www.altodock.com/en/assets/css/profile/fb.png" width="32" height="32" alt="" border="0" />
										</span>
										
										<span class="d-settings-socials__item__title">Facebook</span>
										<span class="d-settings-socials__item__controls">
										
											<a class="d-settings-socials__item__controls__add js-d-social-add" target="_blank" href="<?php echo $userprofile["fb"]; ?>" data-social-name="facebook">></a>
										</span>
									</li>
									<?php } ?>
									<?php 
									if (isset($userprofile["googleplus"])){ ?>
									<li class="d-settings-socials__item js-d-social-item d-settings-socials__item--added" data-social="google">
										<span class="d-settings-socials__item__icon d-settings-socials__item__icon--google">
											<img src="https://www.altodock.com/en/assets/css/profile/googleplus.png" width="32" height="32" alt="" border="0" />
										</span>
										
										<span class="d-settings-socials__item__title">Google+</span>
										<span class="d-settings-socials__item__controls">
									
											<a class="d-settings-socials__item__controls__add js-d-social-add" target="_blank" href="<?php echo $userprofile["googleplus"]; ?>" data-social-name="google">></a>
										</span>
									</li>
									<?php } ?>
									<?php 
									if (isset($userprofile["wechat"])){ ?>
									
									<li class="d-settings-socials__item js-d-social-item " data-social="wechat">
										<span class="d-settings-socials__item__icon d-settings-socials__item__icon--wechat">
										    <img src="https://www.altodock.com/en/assets/css/profile/wechat.png" width="32" height="32" alt="" border="0" />
										</span>
										
										<span class="d-settings-socials__item__title">Wechat</span>
										<span class="d-settings-socials__item__controls">
										
											<a class="d-settings-socials__item__controls__add js-d-social-add" target="_blank" href="<?php echo $userprofile["wechat"]; ?>" data-social-name="wechat">></a>
										</span>
									</li>
									<?php } ?>
									<?php 
									if (isset($userprofile["whatsapp"])){ ?>
									
									<li class="d-settings-socials__item js-d-social-item " data-social="whatsapp">
										<span class="d-settings-socials__item__icon d-settings-socials__item__icon--whatsapp">
										<img src="https://www.altodock.com/en/assets/css/profile/whatsapp.png" width="32" height="32" alt="" border="0" />
										</span>
										
										<span class="d-settings-socials__item__title">Whatsapp</span>
										<span class="d-settings-socials__item__controls">
										
											<a class="d-settings-socials__item__controls__add js-d-social-add" target="_blank" href="<?php echo $userprofile["whatsapp"]; ?>" data-social-name="whatsapp">></a>
										</span>
									</li>
										<?php } ?>
									<?php 
									if (isset($userprofile["ig"])){ ?>
									
									<li class="d-settings-socials__item js-d-social-item " data-social="ig">
										<span class="d-settings-socials__item__icon d-settings-socials__item__icon--ig">
										<img src="https://www.altodock.com/en/assets/css/profile/ig.png" width="32" height="32" alt="" border="0" />
										</span>
										
										<span class="d-settings-socials__item__title">Instagram</span>
										<span class="d-settings-socials__item__controls">
								
											<a class="d-settings-socials__item__controls__add js-d-social-add" target="_blank" href="<?php echo $userprofile["ig"]; ?>" data-social-name="Instagram">></a>
										</span>
									</li>
										<?php } ?>
									<?php 
									if (isset($userprofile["twitter"])){ ?>
									
									<li class="d-settings-socials__item js-d-social-item " data-social="twitter">
										<span class="d-settings-socials__item__icon d-settings-socials__item__icon--twitter">
											<img src="https://www.altodock.com/en/assets/css/profile/twitter.png" width="32" height="32" alt="" border="0" />
											
										</span>
										
										<span class="d-settings-socials__item__title">Twitter</span>
										<span class="d-settings-socials__item__controls">
										
											<a class="d-settings-socials__item__controls__add js-d-social-add" target="_blank" href="<?php echo $userprofile["twitter"]; ?>" data-social-name="twitter">></a>
										</span>
									</li>
										<?php } ?>
									<?php 
									if (isset($userprofile["linkedin"])){ ?>
									<li class="d-settings-socials__item js-d-social-item " data-social="linkedin">
										<span class="d-settings-socials__item__icon d-settings-socials__item__icon--linkedin">
											<img src="https://www.altodock.com/en/assets/css/profile/linkedin.png" width="32" height="32" alt="" border="0" />
											
										</span>
										
										<span class="d-settings-socials__item__title">LinkedIn</span>
										<span class="d-settings-socials__item__controls">
										
											<a class="d-settings-socials__item__controls__add js-d-social-add" target="_blank" href="<?php echo $userprofile["linkedin"]; ?>" data-social-name="linkedin">></a>
										</span>
									</li>
									<?php } ?>
								
									
								</ul>
							</div>
						</div>
					</div>
				</div>
			
			</div>
		
		</div>
	</div>


		
		
	</div>
	
	


<div class="d-footer">
	
	<div class="d-footer-main">
		<div class="d-container container-fluid">
			<div class="row">
			
			
				
				<div class="col-md-6 col-sm-6 col-xs-12 text-left">
				
				
					<div > Powered by <a href="https://www.altodock.com">
            <img src="https://www.altodock.com/en/images/logo.png" width="120" height="34" alt="Logo">
        </a></div>
				
				<?php /*	<ul class="d-footer-links">
						
						<li>
							<div class="langs-wrap js-langs-all">
							<div class="lang-handler js-langs-handler">
								<span class="lang-handler-text js-lang-handler-text">English</span>
								
							</div>
							<div class="langs-list-wrap js-langs-list">
								<ul class="langs-list">
									
									
									
									<li class="lang lang--current">
										<a href="https://www.altodock.com/sites/url/jxfx3dv44/dashBoard/profile#" data-lang="en" class="js-lang">
											<img src="./Dashboard_ Profile_files/en.png" alt="">English
										</a>
									</li>
									
									
									<li class="lang ">
										<a href="https://www.altodock.com/sites/url/jxfx3dv44/dashBoard/profile#" data-lang="pt-BR" class="js-lang">
											<img src="./Dashboard_ Profile_files/pt-BR.png" alt="">繁中
										</a>
									</li>
								
									<li class="lang ">
										<a href="https://www.altodock.com/sites/url/jxfx3dv44/dashBoard/profile#" data-lang="pt-BR" class="js-lang">
											<img src="./Dashboard_ Profile_files/pt-BR.png" alt="">簡中
										</a>
									</li>

									
									
								</ul>
							</div>
						</div>

						</li>
						
						





					</ul> */ ?> 
				</div>
				
			</div>
		</div>
	</div>
</div>





	<div class="UL">
		<p class="ul-tooltip-wrapper ul-tooltip-hidden">
			<span class="ul-tooltip-arr" data-gravity=""></span>
			<span class="ul-tooltip-text"></span>
		</p>
	</div>


</div></noscript>

	<span class="ul-nice-modal__overlay js-nice-modal__overlay js-nice-modal__close"></span>
	
	
	
</div><div class="ul-notify-wrapper ul-notify-wrapper--no-events  UL ul-sp" style="display: none;"></div><div class="ul-alert-wrapper UL ul-sp" style="display: none;"></div><div class="ul-confirm-wrapper UL ul-sp" style="display: none;"></div>

<div class="ul-loader-wrap js-loader-wrap " data-options-img="false">

	

	<div class="ul-loader-content-wrap js-loader-content-wrap ">
		<div class="ul-loader-rings js-loader-rings">
			<span class="ul-loader-ringFour"></span>
			<span class="ul-loader-ringThree"></span>
			<span class="ul-loader-colorRing"></span>
			<span class="ul-loader-ringTwo"></span>
			<span class="ul-loader-ball"></span>
			<span class="ul-loader-cat js-loader-cat"></span>
		</div>
		
		
		<div class="ul-loader-text-wrap">
			
				<span class="ul-loader-title">Loading website</span>
			
			
		</div>
	</div>
</div>
</body>

</html>