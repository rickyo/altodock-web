<!DOCTYPE html>
<html lang="en">


<?php require_once("global_header.php"); ?>

<!-- The #page-top ID is part of the scrolling feature -
the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->
<body id="page-top" data-spy="scroll" data-target="#st-nav">

<?php require_once("page_header.php"); ?>


<!-- Hero Section
================================================== -->
<section id="hero" class="hero-section hero-index" >
    <div class="hero-layer "></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
			
				<div class="hero-index-logo">
					<img src="<?php echo IMG_DIR; ?>/alto-logo.png" alt="logo">
					
                   
                    <div id="particles-js"></div>
					
                </div>
				<div class="overlay-text">
						 <h3 class="wow fadeInLeft animated">Are you ready to upgrade and transform your business one step further?<h3/>
						 <h4 class="wow fadeInLeft animated">Discover how 400+ Companies did that through TVP</h4> 
						
						</p>
					</div>
				
<?php /*					
                <div class="typed-text">
                    <span class="skilltechtypetext">
						<input type="hidden" name="skilltechtypetextval" class="skilltechtypetextval" value="Integrating Technology^,Lower Cost Higher Efficiency^,Join Our Communities^" />
                        <span class="typed-cursor">|</span>
                    </span>
                </div>  */ ?>
				
				<a href="#quote" class="scroll-btn"><button class="btn btn-default btn-lg wow fadeInRight hvr-sweep-to-right button-gradient animated ">Click here to know more for FREE</button></a>
				
            </div>
        </div>
    </div>
</section>
<!-- /.hero-section -->
<?php /*
<!-- Image Section
================================================== -->
<section id="big-image" class="image-section imagebg02">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <h3 class="wow fadeInLeft animated">Struggling on How to Integrate Your Business with Technology?</h3>
                <h4 class="wow fadeInLeft animated"><b>Increase 200% of Your Business Growth with the Right TOOLS</b></h4>
				<h2 class="wow fadeIn animated  sec-text"><span>Want to know the Secret to Unlock the Expansion of Your Business?</span></h2>

                <a href="index.php#quote" class="scroll-btn"><button class="btn btn-default wow fadeInRight hvr-sweep-to-right button-gradient animated " > YES, I Want to Know More</button></a>
			
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
*/ ?>
<!-- /.image section -->
<?php /*
<section id="secret" class="about-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="about-text">
                  
                    <p class="wow fadeIn animated"></p>
           
					<img class="wow fadeInLeft animated" src="<?php echo IMG_DIR; ?>/thumb01.jpg" id="thumb01" width="70%" alt="logo">
                    <!-- /.about-icons -->
                </div>

             
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- /.about-section -->
<!-- /.image section -->
*/ ?>


<section id="quote" class="quote-section">

        <div class="row">
            <div class="col-md-12">
				<div class=" quote-headline">
		
				
					<h4 class="wow fadeIn animated">1 – Integrating With your Business with Technology</span></h4>
				</div>
				 <div class="about-text" style="text-align:center;">
				
				<h3 class="wow fadeInLeft animated" >Struggling on How to Integrate Your Business with Technology? WE ARE HERE to Support You.</h3> <br/>
				<h4 class="wow fadeInLeft animated" >“At least 40% of all businesses will die in the next 10 years… <br/>If they don’t figure out how to change their entire company to accommodate new technologies.” <br/>— JOHN CHAMBERS, EXECUTIVE CHAIRMAN, CISCO SYSTEM</h4> <br/>
				 
					<img class="wow fadeInLeft animated" src="<?php echo IMG_DIR; ?>/thumb01.jpg" id="thumb01" width="70%" alt="logo">
				 </div> 	
		 
			</div>
		</div>
        <!-- /.row -->
  
    <!-- /.container -->
</section>

<section id="quote2" class="quote-section" >
  
        <div class="row">
            <div class="col-md-12">
				<div class=" quote-headline">
					<h4 class="wow fadeIn animated" >2 – Minimize Cost, Maximize Efficiency</span></h4>
				</div>
			 <div class="about-text" style="text-align:center;">
			
				<h3 class="wow fadeInRight animated" >Need to Storage of Your Data? </h3> <br/>
				<h4 class="wow fadeInRight animated">Want to Systemize Your Work? We Help You to Enhance Operation.  </h4> <br/>
					 
				<img class="wow fadeInRight animated" src="<?php echo IMG_DIR; ?>/thumb02.jpg" id="thumb02" width="50%" alt="logo">
			 </div> 	
		 
        </div>
		</div>
        <!-- /.row -->
    
    <!-- /.container -->
</section>

<section id="quote3" class="quote-section" >
  
        <div class="row">
            <div class="col-md-12">
				<div class=" quote-headline">
				    <h4 class="wow fadeIn animated">3 – Looking for Partnership? Join Our Communities</span></h4>
				</div> 
				 <div class="about-text" style="text-align:right;">
					 <div class="col-md-7">
							<h3 class="wow fadeInLeft animated">We Love to Share Our Resources. </h3> <br/>
							<h4 class="wow fadeInLeft animated">Discover the Power of Leveraging.</h4> <br/>
					 </div>
				 	 <div class="col-md-5"  style="text-align:left;">
						<img class="wow fadeInLeft animated" src="<?php echo IMG_DIR; ?>/thumb03.jpg" width="300px" alt="logo">
					 </div>
				 </div> 	
		 
        </div>
		</div>
        <!-- /.row -->
    
    <!-- /.container -->
</section>



<?php /*
<!-- Image Section
================================================== -->
<section id="big-image" class="image-section imagebg01">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <h3 class="wow fadeInLeft animated"><span>Blockchain</span> Technology</h3>
                <h4 class="wow fadeInLeft animated">Secure your payment system with trusted protocol</h4>
                <button class="btn btn-default btn-lg wow fadeInRight hvr-sweep-to-right button-gradient animated">Get it now</button>
			
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- /.image section -->
 */ ?>

<!-- /.about-section -->




<!-- Call To Action Section
================================================== -->
<section id="about" class="call-to-section">
    <div class="call-to-layer dark-gradient"></div>
    <div id="carouselAbout" class="container carousel slide" data-ride="carousel">
		 
        <div class="row">
            <div class="carousel-inner" class="col-md-12">
			    
				<div class="carousel-item active">

					<h3 class="wow fadeIn animated" style="font-weight:bold;">WHO ARE WE?</h3>
					<p class="wow fadeIn animated" style="font-size:20px;">
					
					<!-- Start slider.com BODY section --> <!-- add to the <body> of your page -->
					<div id="slider-container1">
					<div class="ws_images">
					 <ul class="slides">
					
						<?php 
							$about_script=array();
							$about_script[0] =<<<TXT
							We see the mission of awakening the whole society with up-to-date technology and potential workforces.<br/>
							It drives us to help expanding the ideas of every businesses
TXT;
							$about_script[1] =<<<TXT
							We are groups of entrepreneurs, financial elites and technical expertise that accelerate our efforts with innovation and practical solutions. <br/>
							We share ideas and communicate between ourselves and our customers for boosting both business growth.<br/>
TXT;
							$about_script[2] =<<<TXT
							No matter how we are running for our business, our goals hasn't changed, we are now more focusing on create something new and valuable to help the society.<br/><br/>
TXT;
							$about_script[3] =<<<TXT
							We started to chase our dream since 2011, cooperating with different types of business ranging from retailing, E-commerce, in-house software development, investment services to catering. We have learned a lot and gather so many resources that supporting you to success. 
TXT;
							$about_script[4] =<<<TXT
							We believe in using the innovation technical solutions to liberate the ideas of every potential entrepreneurs.
TXT;
							
						?>
						<?php 
						foreach ($about_script as $key => $script){ ?>
							<li>
							<p class="flex-caption"><?php echo $script; ?></p></li>
						<?php } ?>	
					</ul></div>
		
					
					</div>	
					<!-- End slider.com BODY section -->
					
	
					<br/>
					</p>
				</div>	
				
					
            </div>
        </div>
    </div>
</section>
<!-- /.call-to-section -->



<section id="services" class="about-services-section about-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="about-text">
                    <h2 class="wow fadeIn animated"><span>Services</span></h2>
                    <p class="wow fadeIn animated"></p>
           
					
                    <!-- /.about-icons -->
                

                <div class="row">
						 <h3 class="wow fadeInLeft animated">One Stop IT Consultant Services</h3>
						 <h4 class="wow fadeInLeft animated">We build System and Applications for Financial Industries. We can handle Your request.</h4>
						 
						 <br/>
						 <br/>

						 <h3 class="wow fadeInLeft animated">Business and Marketing Consultant</h3>
						 <h4 class="wow fadeInLeft animated">Intergrade Our Experience With Your Needs</h4>
						<br/>
						 <br/>
						 <h3 class="wow fadeInLeft animated">Personal/Company Wealth Management</h3>
						 <h4 class="wow fadeInLeft animated">More than 50%+ Return per Year (Quota Full, Currently unavailable)</h4>
				</div> 
				
				</div>
			</div>
	</div>
</div>
</section>	

<?php /*
<!-- Skills Section
================================================== -->
<section id="services" class="skills-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="skills-headline">
                    <h4 class="wow fadeIn animated"> <span>Services</span></h4>
                    <p class="wow fadeIn animated">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec
                        odio. Praesent libero. Sed
                        cursus ante dapibus diam.</p>
                </div>
            </div>
            <div class="col-md-12 progress-bar-wrapper">
                <div class="progress-bars">
                    <div class="bar-wrapper">
                        <div class="col-md-3 col-sm-3 col-xs-4 wow fadeIn animated">Adobe creative cloud</div>
                        <div class="col-md-8 col-sm-8 col-xs-7">
                            <div class="progressBar skill-bar-gradient wow animated first-bar"></div>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-1">
                            <span class="counter">75</span><span>%</span>
                        </div>
                    </div>
                    <div class="bar-wrapper">
                        <div class="col-md-3 col-sm-3 col-xs-4 wow fadeIn animated">WORDPRESS</div>
                        <div class="col-md-8 col-sm-8 col-xs-7">
                            <div class="progressBar skill-bar-gradient wow animated second-bar"></div>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-1">
                            <span class="counter">80</span><span>%</span>
                        </div>
                    </div>
                    <div class="bar-wrapper">
                        <div class="col-md-3 col-sm-3 col-xs-4 wow fadeIn animated">Laravel</div>
                        <div class="col-md-8 col-sm-8 col-xs-7">
                            <div class="progressBar skill-bar-gradient wow animated third-bar"></div>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-1">
                            <span class="counter">50</span><span>%</span>
                        </div>
                    </div>
                    <div class="bar-wrapper">
                        <div class="col-md-3 col-sm-3 col-xs-4 wow fadeIn animated">GRAPHIC DESIGN</div>
                        <div class="col-md-8 col-sm-8 col-xs-7">
                            <div class="progressBar skill-bar-gradient wow animated fourth-bar"></div>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-1">
                            <span class="counter">70</span><span>%</span>
                        </div>
                    </div>
                    <div class="bar-wrapper">
                        <div class="col-md-3 col-sm-3 col-xs-4 wow fadeIn animated">HTML / CSS / JAVASCRIPT</div>
                        <div class="col-md-8 col-sm-8 col-xs-7">
                            <div class="progressBar skill-bar-gradient wow animated fifth-bar"></div>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-1">
                            <span class="counter">59</span><span>%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.skills-section -->
*/?>

<?php include("contact_us.php"); ?>


<?php include("page_footer.php"); ?>



<?php include("global_footer.php"); ?>


</body>
</html>


