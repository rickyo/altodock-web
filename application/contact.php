
<!-- Contact Section
================================================== -->
<section id="contact" class="contact-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="contact-headline">
                    <h3 class="wow fadeIn animated"> <span>Contact</span> Us</h3>
                    <p class="wow fadeIn animated">Have a question? We’re all ears!</p>
                </div>

                <div class="contact-content">
                    <div class="contact-info">
                        <p class="contact-header"><span>CONNECT</span> WITH US ON</p>
                        <div class="info-line">
                            <div class="info-icon wow fadeIn animated">
                                <i class="fa fa-envelope-o primary-white" aria-hidden="true"></i>
                            </div>
                            <div class="info-text wow fadeIn animated">
                                <p>info@altodock.com</p>
                            </div>
                        </div>
                        <div class="info-line">
                            <div class="info-icon wow fadeIn animated">
                                <i class="fa fa-phone primary-white" aria-hidden="true"></i>
                            </div>
                            <div class="info-text wow fadeIn animated">
                                <p>+852 9615 2179</p>
                            </div>
                        </div>

                        <div class="info-line">
                            <div class="info-icon wow fadeIn animated">
                                <i class="fa fa-address-card-o primary-white" aria-hidden="true"></i>
                            </div>
                            <div class="info-text wow fadeIn animated">
                                <p>UNIT 728, 7/F, REALITY TOWER,<br/>4 SUN ON STREET, CHAI WAN
								</p>
                            </div>
                        </div>

                        <div class="info-line">
                            <div class="info-icon wow fadeIn animated">
                                <i class="fa fa-map-marker primary-white" aria-hidden="true"></i>
                            </div>
                            <div class="info-text wow fadeIn animated">
                                <p>HONG KONG</p>
                            </div>
                        </div>

                        <div class="bottom-info">
                            <h4 class="wow fadeIn animated"><span>Connect</span> with us</h4>

                            <ul class="list-inline social-icons wow fadeIn animated">
                                <li>
                                    <a href="index.php#" class="primary-2-back primary-back-hover">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="index.php#" class="primary-2-back primary-back-hover">
                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="index.php#" class="primary-2-back primary-back-hover">
                                        <i class="fa fa-google-plus" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="index.php#" class="primary-2-back primary-back-hover">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul>

                            <p class="wow fadeIn animated">Keep up with the latest company news and events. </p>
                        </div>
                    </div>

                    <div class="contact-form">
                        <h4 class="wow fadeIn animated">Send us a <span>Message</span></h4>

                        <form method="post" >
                            <div class="form-group wow fadeIn animated">
                                <label for="name">Name: </label>
                                <input type="text" id="name" name="name" class="form-control">
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group wow fadeIn animated">
                                <label for="email">Email: </label>
                                <input type="email" id="email" name="email" class="form-control">
                            </div>
                            <!-- /.form-group -->

							<div class="form-group wow fadeIn animated">
                                <label for="topic">I want to know more about… </label>
								<select id="topic" name="topic"  class="form-control selector">
									<option value="TVP Application">TVP Application</option>
									<option value="IT Consultant">IT Consultant</option>
									<option value="Expanding my Business">Expanding my Business</option>
									<option value="Marketing Solution">Marketing Solution</option>
									<option value="Others">Others</option>
								</select>
                            </div>

                            <div class="form-group wow fadeIn animated">
                                <label for="message">Message: </label>
                                <textarea id="message" name="message" class="form-control" rows="4"></textarea>
                            </div>
                            <!-- /.form-group -->


                            <button class="btn btn-default wow fadeIn primary-back-hover primary-hover-border animated" type="submit">Submit</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.contact-section -->

			  
<script>

$(function() {
    $("#contact button").click(function() {
		
		var url = "sendmail";
        var dataset ={

			name : $("#name").val(),
			email : $("#email").val(),
			topic : $("#topic").val(),
			msg : $("#message").val()
		};
		$.post(
			url,
			dataset,
			function(json) {
				
				if (json.ok == 1){
					alertify.success("Email Sent");
				}else{
					alertify.error("Sending Failure. Please check whether the email address provided is valid");
				}
			}).fail( function(jqXHR, textStatus, errorThrown) {
					console.log(textStatus);
					alertify.error("Sending Failure.  Please check whether the email address provided is valid");
   
			});

        return false;
    });
});
</script>