<!DOCTYPE html>
<html lang="en">


<?php require_once("global_header.php"); ?>

<!-- The #page-top ID is part of the scrolling feature -
the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->
<body id="page-top" data-spy="scroll" data-target="#st-nav">

<?php require_once("page_header.php"); ?>

<!-- Hero Section
================================================== -->
<?php /*

<section id="hero" class="hero-section">
    <div class="hero-layer "></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="hero-logo">
                    <img src="<?php echo IMG_DIR; ?>/omni-logo.png" alt="logo">
                    <div id="particles-js"></div>
					
                </div>
                <div class="typed-text">
                    <span class="skilltechtypetext">
                        <span class="typed-cursor">|</span>
                    </span>
                </div>
				<button id="trading_btn" class="btn btn-default btn-lg wow fadeInRight hvr-sweep-to-right primary-2-back animated">Explore more</button>
            </div>
        </div>
    </div>
</section> */ ?>
<!-- /.hero-section -->

<!-- About Us Section
================================================== -->
<section id="about" class="about-section tvp-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="about-text">
                    <h2 class="wow fadeIn animated  sec-text"><span>Want to save 66% cost for expanding your business with<br/><b style="color:#9600f4"> TVP</b> Application Services?</span></h2>
                    
                    
					<p class="wow fadeIn animated">We are here to serve you with our integrated services and <br/> reliable application on latest technology with efficient approach. </p>
					
					
					<ul class="about-icons list-inline">
					   <li class="wow fadeIn animated">
                            <div class="icon"><i class="material-icons primary">beenhere</i></div>
                            <div class="text">CRM & ERP system</div>
                        </li>
						<li class="wow fadeIn animated">
                            <div class="icon"><i class="material-icons primary">insert_chart</i></div>
                            <div class="text">IoT & mobile application</div>
                        </li>
                        <li class="wow fadeIn animated">
                            <div class="icon"><i class="material-icons primary">comment</i></div>
                            <div class="text">eCommerce & Online shopping cart</div>
                        </li>
						<li class="wow fadeIn animated">
                            <div class="icon"><i class="material-icons primary">comment</i></div>
                            <div class="text">FinTech</div>
                        </li>
					</ul>	
                    <!-- /.about-icons -->
                </div>

                <div class="about-images tvp-images">
                    <div class="mockup image-left">
                        <img class="wow fadeInLeft animated" src="<?php echo IMG_DIR; ?>/ipads-left.png" alt="ipad">
                    </div>
                    <!-- /.image-left -->

                    <div class="mockup image-center">
                        <img class="wow slideInUp animated" src="<?php echo IMG_DIR; ?>/ipads-center.png" alt="ipad">
                    </div>
                    <!-- /.image-center -->

                    <div class="mockup image-right">
                        <img class="wow fadeInRight animated" src="<?php echo IMG_DIR; ?>/ipads-right.png" alt="ipad">
                    </div>
                    <!-- /.image-right -->
                </div>
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- /.about-section -->

<!-- Quote Section
================================================== -->
<section id="tvp-intro" class="quote-section" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 quote-headline">
                <h4 class="wow fadeIn animated">1. What is TVP?</span></h4>
            </div> 
		<div class="about-text" >
		
		
		  <div class="col-md-7">
		  <div class="wow fadeIn  animated" id="quote-para1"><br><br>
				
			TVP (Technology Voucher Programme) was launched in November 2016 on a pilot basis to subsidise local small and medium enterprises (SMEs) in using technological services and solutions to improve productivity, or upgrade or transform their business processes. <br/><br/>
			
			Funding ceiling per applicant is maximum <b>$400,000</b> (Two-third of the project cost)<br/><br/>
		 
	
			Local entities (except listed enterprises) fulfilling the following requirements are eligible to apply for funding under the TVP:<br/>
			
				<ul >
				<li> Registered in Hong Kong under the Business Registration Ordinance (Cap. 310) </li>
				<li>With substantive business operation in Hong Kong which is related to the project under application at the time of application.</li>
				<li>Not a government subvented organisation or subsidiary of any government subvented organisation;</li>
				</ul>
				
			</div>
		 </div>
		 <div class="col-md-5">
           	<img class="wow fadeInRight animated" src="<?php echo IMG_DIR; ?>/tvp01.jpg"  alt="design">
		 </div>
        </div>
		</div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- /.quote-section -->

<!-- Quote Section
================================================== -->
<section id="tvp-apply" class="quote-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 quote-headline">
                <h4 class="wow fadeIn animated">2. How to Apply?</span></h4>
            </div>
           <div class="about-images" style="text-align:center;">
		 
           	<img class="wow fadeInRight animated" src="<?php echo IMG_DIR; ?>/tvp02.jpg" style="width:70%;" alt="design">

        </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- /.quote-section -->

<!-- Quote Section
================================================== -->
<section id="tvp-quote" class="quote-section" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 quote-headline">
                <h4 class="wow fadeIn animated">3. What technology should I use?</span></h4>
            </div>
           <div class="about-images" style="text-align:center;">
			<div class="col-md-8 ">
				<img class="wow fadeInRight animated" src="<?php echo IMG_DIR; ?>/tvp04.jpg" style="width:100%;" alt="design">
			</div>
			 <div class="col-md-4">
			<img class="wow fadeInRight animated" src="<?php echo IMG_DIR; ?>/altobg02.gif" style="width:100%; padding-top:70px;" alt="design">
			</div>

        </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- /.quote-section -->



<!-- Call To Action Section
================================================== -->
<section id="call-to" class="call-to-section">
    <div class="call-to-layer main-gradient"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="wow fadeIn animated"><span>Apply</span> Now!</h3>
                <p class="wow fadeIn animated">
                    For more information on TVP?</p>
                			<br/>
					<img class="wow fadeInLeft animated" src="<?php echo IMG_DIR; ?>/tvp03.jpg" style="width:100%;" alt="design">
				<a href="https://www.itf.gov.hk/l-eng/TVP.asp" target="blank"><button class="btn btn-default wow fadeInRight hvr-sweep-to-right button-gradient animated">Learn More</button></a>
            </div>
        </div>
    </div>
</section>
<!-- /.call-to-section -->

<!-- Skills Section
================================================== -->
<section id="digital" class="digital-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="skills-headline">
                    <h2 class="wow fadeIn animated"><b>Looking for Digital Design? </b></h2>
                    <p class="wow fadeIn animated">Logos, Business Card, Accessories. Stylish your life.</p>
                </div>
            </div>
			<div class="about-images">
				
					<img class="wow fadeInLeft animated" src="<?php echo IMG_DIR; ?>/tvpdesign02.jpg" alt="design">
			</div>
          
        </div>
    </div>
</section>
<!-- /.skills-section -->



<?php include("page_footer.php"); ?>


<?php include("global_footer.php"); ?>

</body>
</html>


