<?php

header('Content-Type: application/json; charset=utf-8');

/**
 * This example shows sending a message using PHP's mail() function.
 */
//Import the PHPMailer class into the global namespace
//use PHPMailer\PHPMailer\PHPMailer;
include "common.php"; 
use PHPMailer\PHPMailer\PHPMailer;

$json = array();

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Asia/Hong_Kong');
//require '../vendor/autoload.php';
//Create a new PHPMailer instance
$mail = new PHPMailer;
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 0;
//Set the hostname of the mail server
$mail->Host = 'altodock.com';
//Set the SMTP port number - likely to be 25, 465 or 587
$mail->Port = 25;
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication
$mail->Username = 'info@altodock.com';
//Password to use for SMTP authentication
$mail->Password = '1Qazxsw2=';
//Set who the message is to be sent from
$mail->setFrom('info@altodock.com', 'Altodock');
//Set an alternative reply-to address
//$mail->addReplyTo('replyto@example.com', 'First Last');
//Set who the message is to be sent to
$mail->addAddress($_REQUEST["email"], $_REQUEST["name"]);
$mail->addBCC('info@altodock.com', 'Altodock');
//Set the subject line
$mail->Subject = "[".$_REQUEST["topic"]."] Thank you for messaging us.";
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body

$contents = file_get_contents('mailtpl\contents.html');

$contents = str_replace("__NAME__", $_REQUEST["name"], $contents);
$contents = str_replace("__EMAIL_CONTENTS__", $_REQUEST["msg"], $contents);

$mail->msgHTML($contents, __DIR__);
//Replace the plain text body with one created manually
$mail->AltBody = 'This is a plain-text message body';
//Attach an image file
//$mail->addAttachment('images/phpmailer_mini.png');
//send the message, check for errors
if (!$mail->send()) {
    $json["respond"] = 'Mailer Error: ' . $mail->ErrorInfo;
	$json_ok = -1;
} else {
    $json["respond"] =  'Message sent!';
	$json_ok = 1;
}

$json["ok"] = $json_ok;
echo json_encode($json);
exit;