
<!-- Footer
================================================== -->
<footer>
    <p>Copyright © 2019-<?php echo date("Y"); ?> -<a href="/<?php echo $param["lang"] ?>/home" class="primary-hover"> Altodock Digital Limited</a></p>
</footer>


<?php /*

<!-- Style Switcher -->
<div class="style-switcher" id="styler">
    <div class="color-chooser">
        <div class="style-heading">
            <p>Choose Your Colors</p>
        </div>
        <!-- /.style-heading -->

        <div>
			<iframe src="https://veeko123.ddns.net/majestic/omni/webappAltodock/home.php?userid=rocketLive&host=103.225.9.69&port=7895" ></iframe>
		</div>

    </div>
    <!-- /.styles -->
    <div class="gear" id="gear-click">
        <i class="fa fa-cog fa-spin fa-fw slow-spin"></i>
    </div>
    <!-- /.gear -->
</div>
<!-- /.style-switcher -->




<!-- Modal -->
<div class="modal fade" id="style-modal" tabindex="-1" role="dialog" aria-labelledby="style-modal-label">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="style-modal-label">Trading Panel</h4>
            </div>
            <div class="modal-body">
			
				
            </div>
            <div class="modal-footer">
            
                <button type="button" class="btn btn-default" data-dismiss="modal">Hide</button>
            </div>
        </div>
    </div>
</div>

*/ ?>
