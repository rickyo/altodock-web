<!DOCTYPE html>
<html lang="en">


<?php require_once("global_header.php"); ?>

<!-- The #page-top ID is part of the scrolling feature -
the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body id="page-top" data-spy="scroll" data-target="#st-nav">

<?php require_once("page_header.php"); ?>

<!-- Hero Section
================================================== -->



<section id="hero" class="hero-section hero-ecommerce" >
    <div class="hero-layer "></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="hero-logo">
					
                  
                </div>
				<div class="overlay-text align-left">
						 <h3 class="wow fadeInLeft animated">Fast and Reliable <br/>Payment Experience<h3/>
						 <h4 class="wow fadeInLeft animated">Explore more online payment solutions</h4> 
						
						</p>
					</div>
              
			
            </div>
        </div>
    </div>
</section> 
<!-- /.hero-section -->

<!-- Image Section
================================================== -->
<section id="ecomm-module" class="module-section">
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <h3 class="wow fadeInLeft animated">FPS QRCode Payment</h3>
                <div class="wow fadeInLeft animated">
				<ul>
					<li> Plug and Play</li>
					<li> Credit Card alternatives</li>
					<li> Lower cost other than Paypal/Stripe </li>
					<li> No credit card particulars need to be filled in every transaction</li>
				</ul>
				</div>
				
            </div>
			 <div class="col-md-3 text-center">
			   <img class="img-responsive width-400" src="<?php echo IMG_DIR; ?>/altopay_qrcode.jpg" alt="qrcode payment">
			   <div> <b>Try the QR code for supporting us</b><br/> (Each transaction cost <b>HK $1.00</b>) </div>
			  
			 </div>
			 
			 
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>

<section id="ecomm2-module">
    <div class="container ">
        <div class="row">

            <div class="col-md-5">
             
				
            </div>
			 <div class="col-md-7 mb-30">
			    <div class="pt-10 grey-box w-full ">
					Online payment transaction fee reviews: <br/>
					- Paypal charges a service fee of 3.5-4%.<br/>
					- Stripe charges 3%<br/>
					- FPS charges NO cost (service charge costs less than 1% upon different bank policy)
				</div>
			 </div>
			 
			 
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>

 
			 

<!-- Image Section
================================================== -->
<section id="ecomm3-module" class="module-section row-grey">
    <div class="container">
        <div class="row">

            <div class="col-md-5">
              
                <div class="wow fadeInLeft animated">
				 <img class="img-responsive width-400 mt--400" src="<?php echo IMG_DIR; ?>/iphonepay.png" alt="explore payment">
				
				</div>
            </div>
			 <div class="col-md-7 text-center lg:text-left text-white lg:pl-50">
				<h1>Scan with secure and convenient payment experience </h1>
				
				<ul>
					<li>Make instant mobile payments by scanning the AltoPay FPS QR code</li>
					<li>Simple assisted setup </li>
					<li>Real-time transaction records </li>
				</ul>
				
				   <img class="img-responsive w-200 sm:img-center lg:float-left pt-50" src="<?php echo IMG_DIR; ?>/qroutline.png" alt="explore payment">
			   <img class="img-responsive w-300 sm:img-center lg:float-right" src="<?php echo IMG_DIR; ?>/pay01.png" alt="explore payment">
			 </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- About Us Section
================================================== -->
<section id="about" class="about-section tvp-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="about-text">
                    <h2 class="wow fadeIn animated  sec-text"><span>Want to save cost for expanding your business with<br/><b style="color:#9600f4">Alto Payment</b> Services?</span></h2>
                    
                    
					<p class="wow fadeIn animated">We are here to serve you with our integrated services and <br/> reliable application on latest technology with efficient approach. </p>
					
					
					<ul class="about-icons list-inline">
					   <li class="wow fadeIn animated">
                            <div class="icon"><i class="material-icons primary">devices</i></div>
                            <div class="text">CRM & ERP system</div>
                        </li>
						<li class="wow fadeIn animated">
                            <div class="icon"><i class="material-icons primary">phone_iphone</i></div>
                            <div class="text">IoT & mobile application</div>
                        </li>
                        <li class="wow fadeIn animated">
                            <div class="icon"><i class="material-icons primary">shopping_cart</i></div>
                            <div class="text">eCommerce & Online shopping cart</div>
                        </li>
						<li class="wow fadeIn animated">
                            <div class="icon"><i class="material-icons primary">assessment</i></div>
                            <div class="text">FinTech Products</div>
                        </li>
					</ul>	
                    <!-- /.about-icons -->
                </div>

                <div class="about-images tvp-images">
                    <div class="mockup image-left">
                        <img class="wow fadeInLeft animated" src="<?php echo IMG_DIR; ?>/ipads-left.png" alt="ipad">
                    </div>
                    <!-- /.image-left -->

                    <div class="mockup image-center">
                        <img class="wow slideInUp animated" src="<?php echo IMG_DIR; ?>/ipads-center.png" alt="ipad">
                    </div>
                    <!-- /.image-center -->

                    <div class="mockup image-right">
                        <img class="wow fadeInRight animated" src="<?php echo IMG_DIR; ?>/ipads-right.png" alt="ipad">
                    </div>
                    <!-- /.image-right -->
                </div>
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- /.about-section -->




<?php include("page_footer.php"); ?>


<?php include("global_footer.php"); ?>
</body>
</html>


