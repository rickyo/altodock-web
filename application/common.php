<?php

// Php mailer
include "phpmailer/PHPMailer.php"; 
include "phpmailer/SMTP.php";
include "phpmailer/POP3.php";
include "phpmailer/OAuth.php";
include "phpmailer/Exception.php";


// Language config
$lang = "en";
include "language/common.lang.php";


function getFullURLPath($lang){
	if (!defined('_LOCALHOST')){
		return $lang;
	}else{		
		return _LOCALE."/".$lang;
	}
}

?>
