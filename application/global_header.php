
<?php include("common.php"); ?>

<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' name='viewport'/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Altodock – IT Development with Practical Business Ideas</title>

	<link rel="icon" type="image/png" href="https://www.altodock.com/en/favicon-32x32.png" />
	<!-- <link rel="icon" href="favicon.ico?v=2" type="image/x-icon"> -->

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700%7CTitillium+Web:200,300,400,600,700&amp;subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Code+Pro" rel="stylesheet">
    <link href="<?php echo FONTS_DIR; ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Libraries CSS -->
    <link href="<?php echo CSS_DIR; ?>/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo CSS_DIR; ?>/animate.min.css" rel="stylesheet">
    <link href="<?php echo CSS_DIR; ?>/bootstrap-colorpicker.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo CSS_DIR; ?>/particle_light.css?v1.0" rel="stylesheet">
    <link href="<?php echo CSS_DIR; ?>/particle_demo.css" rel="stylesheet">
    <link href="<?php echo CSS_DIR; ?>/skins.css" rel="stylesheet">

	<link href="<?php echo CSS_DIR; ?>/custom-style.css?v=1.3" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<link href="<?php echo CSS_DIR; ?>/wowslider.css?v1.0" rel="stylesheet">
	
	<link rel="stylesheet" href="<?php echo PLUGIN_DIR; ?>/alertify/themes/alertify.core.css" />
	<link rel="stylesheet" href="<?php echo PLUGIN_DIR; ?>/alertify/themes/alertify.default.css" id="toggleCSS" />
	<script src="<?php echo PLUGIN_DIR; ?>/alertify/lib/alertify.min.js"></script>
	
	<?php 
		if (isset($css_list)){
			foreach ($css_list as $csshref){ ?>
				<link rel="stylesheet" href="<?php echo $csshref; ?>" />
	<?php	}
		} ?>
	
<script
src="https://code.jquery.com/jquery-3.4.1.js"
			  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
			  crossorigin="anonymous"></script>
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-143309525-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-143309525-1');
	</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
Tawk_API.onLoad = function(){
	hideBrand();
  };
 Tawk_API.onChatMaximized = function(status){
    hideBrand();
};
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d2476307a48df6da243ab85/1dfbhfibb';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();

function hideBrand(){
	var node = "";
	for(i=0;i<25;i++){
		try{
			node = $('iframe').get(i).contentWindow.document;
			if(node.getElementsByClassName("emojione")[0]!=undefined){
				break;
			}
		}catch(err){
		}
	}
	node.getElementsByClassName("emojione")[0].parentNode.parentNode.setAttribute("style","display:none");
	$("#popoutChat",node).attr("style","display:none");
	if($("#cobrowse",node).length==0){
		$("#popoutChat",node).parent().append("<li id=\"cobrowse\">Cobrowse</li>");
		$("#cobrowse",node).bind("click",function(event){parent.cobrowse();parent.Tawk_API.minimize();});
	}
}
function cobrowse(){
	TogetherJS(this); return false;
}
</script>
<!--End of Tawk.to Script-->
<!-- cobrowse plugin -->
<script src="https://togetherjs.com/togetherjs-min.js"></script>

</head>