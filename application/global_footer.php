

<!-- Scripts
================================================== -->
<!-- JavaScript Libraries -->
<script src="<?php echo JS_DIR; ?>/libs.min.js"></script> 

<!-- Custom Particle Theme JavaScript -->
<script src="<?php echo JS_DIR; ?>/particle-theme.min.js"></script>
<script defer src="<?php echo JS_DIR; ?>/jquery.flexslider.js"></script>
<!--<script src="assets/js/skilltech-style-switcher.min.js"></script> -->


<script src="<?php echo JS_DIR; ?>/skilltech-style-switcher.min.js"></script>

<script>

var $root = $('html, body');

$(document).ready(function(){
	$('.scroll-btn').click(function() {
		var href = $(this).attr('href');
		
		//split the url by # and get the anchor target name - home in mysitecom/index.htm#home
        var parts = href.split("#");
        var trgt = parts[1];
		
		//console.log(trgt);

		$root.animate({
			scrollTop: $("#"+trgt).offset().top
		}, 500, function () {
			window.location.hash = href;
		});

		return false;
	});
	
	
	 $("#slider-container1").flexslider({
		animation: "slide"
	  });
	

	
	
});
</script>
