
<!-- Navigation Menu
================================================== -->
<?php $lang = $_SESSION["lang"];
if (!isset($param["lang"])){$param["lang"] = "en";}

 ?>
<header>
    <div id="st-logo">
        <a href="/<?php echo getFullURLPath($param["lang"]) ?>/home">
            <img src="<?php echo IMG_DIR; ?>/logo.png" alt="Logo">
        </a>
    </div>
	
		<div class="social">
				<div class="fa-icons">
							

				</div>
		<div class="soc-right">
				<span> <i class="fa fa-whatsapp" aria-hidden="true"></i><a href="https://wa.me/85296152179?text=I'm%20interested%20in%20TVP%20Application."><font size="3" > +852 9615 2179</font></a></span>						
				
				<span><i class="fa fa-envelope " aria-hidden="true"></i><a href="mailto:info@altodock.com"><font size="3" > info@altodock.com</font></a></span>				
					
				<span>
				<ul class="pannel-inline lang js-lang">
    
					<li class="pannel-item_line active" data-lang="zh_tw">繁</li>
				
					<li class="pannel-item_line" data-lang="zh_cn">简</li>          
				
					<li class="pannel-item_line" data-lang="en">EN</li>
				
				</ul>
				</span>
		</div>
		
		<div class="clear"></div>
	</div>


<div id="st-nav">
    <a  class="st-nav-trigger">
        Menu<span></span>
    </a>

    <nav id="st-main-nav">
        <ul>
         <?php /*   <li>
                <a class="page-scroll hvr-underline-from-center" href="index.php"><?php echo $__[$lang]["home"]; ?></a>
            </li> */ ?>
			<?php /*<li>
                <a class="page-scroll hvr-underline-from-center" href="/<?php echo getFullURLPath($param["lang"]) ?>/tvp"><?php echo $__[$lang]["tvp"]; ?></a>
            </li> */ ?>
            <li>
                <a class="page-scroll hvr-underline-from-center" href="/<?php echo getFullURLPath($param["lang"]) ?>/trading"><?php echo $__[$lang]["trading"]; ?></a>
            </li>
			   <li>
                <a class="page-scroll hvr-underline-from-center" href="/<?php echo getFullURLPath($param["lang"]) ?>/ecommerce"><?php echo $__[$lang]["ecommerce"]; ?></a>
            </li>			
            <li>
                <a class="page-scroll hvr-underline-from-center" href="/<?php echo getFullURLPath($param["lang"])?>/home#about"><?php echo $__[$lang]["about_us"]; ?></a>
            </li>
            <li>
                <a class="page-scroll hvr-underline-from-center" href="/<?php echo getFullURLPath($param["lang"]) ?>/home#services"><?php echo $__[$lang]["services"]; ?></a>
            </li>
            <li>
                <a class="page-scroll hvr-underline-from-center" href="/<?php echo getFullURLPath($param["lang"]) ?>/home#contact"><?php echo $__[$lang]["contact_us"]; ?></a>
            </li>
        </ul>
    </nav>
	

	
	
</div>
<!-- /#st-nav -->
</header>