<!DOCTYPE html>
<html lang="en">


<?php require_once("global_header.php"); ?>

<!-- The #page-top ID is part of the scrolling feature -
the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->
<body id="page-top" data-spy="scroll" data-target="#st-nav">

<?php require_once("page_header.php"); ?>

<!-- Hero Section
================================================== -->

<section id="hero" class="hero-section hero-trading" >
    <div class="hero-layer "></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="hero-logo">
                    <img src="<?php echo IMG_DIR; ?>/omni-logo.png" alt="logo">
                    <div id="particles-js"></div>
					
                </div>
                <div class="typed-text">
                    <span class="skilltechtypetext">
						<input type="hidden" name="skilltechtypetextval" class="skilltechtypetextval" value="Top Investing Methodology^,Exclusive Market Signal^,Easy Online Trading^" />
                        <span class="typed-cursor">|</span>
                    </span>
                </div>
				<button id="trading_btn" class="btn btn-default btn-lg wow fadeInRight hvr-sweep-to-right button-gradient animated"><a href="#our-work">Explore more</a></button>
            </div>
        </div>
    </div>
</section>
<!-- /.hero-section -->





<!-- Our Work Section
================================================== -->
<section id="our-work" class="our-work-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 see-work">
                <h2 class="wow fadeInLeft animated">Omni Trading Solution</h2>
            </div>
            <div class="col-md-12 new-style">
                <p class="wow fadeInLeft animated"> 4 Core Modules to trade for highly performance</p>
            </div>
            <div class="col-md-12 portfolio-images">
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="portfolio-img first">
                        <img class="img-responsive" src="<?php echo IMG_DIR; ?>/module01.jpg" alt="porftofilio thumbnail">
                        <div class="sweep-left image-gradient"></div>
                        <div class="sweep-from-center">
                            <div class="sweep-layer">
                                <div class="sweep-content">
                                    <div class="sweep-icon">
                                        <a href="#wise-module">
                                            <i class="fa fa-chevron-circle-down primary-3" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <h3><a href="#wise-module" class="secondary-hover">WISE Core System</a></h3>
                                    <p>Integrated Investing Platform</p>
                                </div>
                            </div>
                        </div>
                        <div class="sweep-right image-gradient"></div>
                    </div>
                    <!-- /.portfolio-img -->
                </div>

                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="portfolio-img second">
                        <img class="img-responsive" src="<?php echo IMG_DIR; ?>/module02.jpg" alt="porftofilio thumbnail">
                        <div class="sweep-left image-gradient"></div>
                        <div class="sweep-from-center">
                            <div class="sweep-layer">
                                <div class="sweep-content">
                                    <div class="sweep-icon">
                                        <a href="#algo-module">
                                            <i class="fa fa-chevron-circle-down primary-3" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <h3><a href="#algo-module" class="secondary-hover">ALGO module</a></h3>
                                    <p>Integllient Trading Robot</p>
                                </div>
                            </div>
                        </div>
                        <div class="sweep-right image-gradient"></div>
                    </div>
                    <!-- /.portfolio-img -->
                </div>

                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="portfolio-img third">
                        <img class="img-responsive" src="<?php echo IMG_DIR; ?>/module03.jpg" alt="porftofilio thumbnail">
                        <div class="sweep-left image-gradient"></div>
                        <div class="sweep-from-center">
                            <div class="sweep-layer">
                                <div class="sweep-content">
                                    <div class="sweep-icon">
                                        <a href="#omni-module">
                                            <i class="fa fa-chevron-circle-down primary-3" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <h3><a href="#omni-module" class="secondary-hover">OMNI Trading App</a></h3>
                                    <p>High Mobility</p>
                                </div>
                            </div>
                        </div>
                        <div class="sweep-right image-gradient"></div>
                    </div>
                    <!-- /.portfolio-img -->
                </div>

                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="portfolio-img fourth">
                        <img class="img-responsive" src="<?php echo IMG_DIR; ?>/module04.jpg" alt="porftofilio thumbnail">
                        <div class="sweep-left image-gradient"></div>
                        <div class="sweep-from-center">
                            <div class="sweep-layer">
                                <div class="sweep-content">
                                    <div class="sweep-icon">
                                        <a href="#chart-module">
                                            <i class="fa fa-chevron-circle-down primary-3" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <h3><a href="#chart-module" class="secondary-hover">Analytical Chart</a></h3>
                                    <p>Specialized Market Signal</p>
                                </div>
                            </div>
                        </div>
                        <div class="sweep-right image-gradient"></div>
                    </div>
                    <!-- /.portfolio-img -->
                </div>

            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.our-work-section -->

<!-- Image Section
================================================== -->
<section id="wise-module" class="module-section">
    <div class="container">
        <div class="row">

            <div class="col-md-6">
                <h3 class="wow fadeInLeft animated">Wise Core System</h3>
                <div class="wow fadeInLeft animated">
				<ul>
				<li> Automatic Trading platform</li>
				<li> API Support</li>
				<li> Datafeed integration</li>
				<li> Multiple Algo integration</li>
				<li> Multiple broker supported</li>
				<li> Backtest supported</li>
				<li> Friendly and intuitive UI</li>
				<li> Trading signal screen</li>
				<li> Easy administration interface</li>
				<li> Logging supported</li>
				</ul>
				</div>
            </div>
			 <div class="col-md-6">
			   <img class="img-responsive" src="<?php echo IMG_DIR; ?>/modb01.jpg" alt="porftofilio thumbnail">
			 </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- /.image section -->

<!-- Image Section
================================================== -->
<section id="algo-module" class="module-section grey">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <h3 class="wow fadeInLeft animated">ALGO Module</h3>
                <div class="wow fadeInLeft animated">
				Automated system using pre-programmed trading instructions to do calculation and do decisionmakings for the potential best trade. <br/><br/>
There are available 3 Algos:<br/>
<ul>
<li>Extrema</li>
<li>“Elliott Wave Theory” core principle </li>
<li>Optimized to catch market turning point based on price and volume</li>
<li> Shadow</li>
<li> VWAP and TWAP core principle</li>
<li>Intelligent mathematical model following market trend</li>
<li>Skynet</li>
<li>Mean reversion core principle</li>
<li>Optimized for swing market</li>
<li>Intelligent bet control base on equilibrium price to optimize profit and reduce loss</li>
</ul>
				</div>
				Algo Performance
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- /.image section -->

<!-- Image Section
================================================== -->
<section id="omni-module" class="module-section">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <h3 class="wow fadeInLeft animated">Omni Trader</h3>
                <div class="wow fadeInLeft animated">
				An investment tool for instant future trading with Algo assistant<br/>
<ul>
<li>Mobile App</li>
<li>Support iOS from iPhone4 to iPhoneX</li>
<li>Support social trading platform</li>
<li>Web</li>
<li>Administrative mode for handling
multi account</li>
<li>Advanced technical analysis and
trading options for professional
traders</li>
</ul>
				</div>
            </div>
			 <div class="col-md-12">
			   <img class="img-responsive" src="<?php echo IMG_DIR; ?>/modb03.jpg" alt="porftofilio thumbnail">
			 </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- /.image section -->


<!-- Image Section
================================================== -->
<section id="chart-module" class="module-section grey">
    <div class="container">
        <div class="row">
		<div class="col-md-6">
		  <h3 class="wow fadeInLeft animated">Analytical Chart</h3>
			   <img class="img-responsive" src="<?php echo IMG_DIR; ?>/modb04.jpg" alt="porftofilio thumbnail">
			 </div>
            <div class="col-md-6">
              
                <div class="wow fadeInLeft animated">
				A web chart showing statistical data for
real time future trade<br/>
<ul>
	<li> Super VWAP</li>
<li> Indicator based on calculation of future
price, volume average and time average</li>
<li> Shows potential market volatility</li>
<li> Skynet Chart</li>
<li> Market data feed to provide the Support
and Resistance calculated by stocks and
options market data.</li>
</ul>
				</div>
            </div>
			
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- /.image section -->

<!-- Quote Section
================================================== -->
<section id="tradequote" class="quote-section" >
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 quote-headline">
                <h4 class="wow fadeIn animated">The next generation <span>Trading Platform</span></h4>
            </div>
            <div class="col-md-12 quote-bottom">
                <div class="container">
                    <h3 class="wow fadeIn animated">
                        One direction, many way out
                    </h3>
                   <p class="wow fadeIn animated"><span>How to follow with the trading flow?</span></p>
				   <a href="#services" class="scroll-btn"><button class="btn btn-default btn-lg wow fadeInRight hvr-sweep-to-right button-gradient animated ">Click here to know more</button></a>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- /.quote-section -->



<!-- Services Section
================================================== -->
<section id="services" class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 service-headline">
                <h2 class="wow fadeIn animated">Omni Trader App</h2>
                <p class="wow fadeIn animated">Trade Everywhere. Trade Everytime</p>
            </div>

            <div class="col-md-12 service-content">
			   <div class="service-image-mobile">
                    <img src="<?php echo IMG_DIR; ?>/omniInstruction.png" alt="iphone">
                </div>
                <div class="left">
                    <div class="service wow fadeIn animated">
                        <div class="text">
                            <a href="service-item.html" class="primary-hover">Real Time Market Data Feed</a>
                            <p>Instant future market index with basic OHCL .</p>
                        </div>
                        <div class="icons">
                            <i class="material-icons primary">create</i>
                        </div>
                    </div>
                    <div class="service wow fadeIn animated">
                        <div class="text">
                            <a href="service-item.html" class="primary-hover">Market Chart</a>
                            <p>Dynamic candle-stick chart with different timeframe</p>
                        </div>
                        <div class="icons">
                            <i class="material-icons primary">screen_share</i>
                        </div>
                    </div>
                    <div class="service wow fadeIn animated">
                        <div class="text">
                            <a href="service-item.html" class="primary-hover">Intuitive Control Panel</a>
                            <p>WYSIWYG design for traders. Less training time, more effectiveness</p>
                        </div>
                        <div class="icons">
                            <i class="material-icons primary">devices</i>
                        </div>
                    </div>
                </div>
                <div class="service-image">
                    <img src="<?php echo IMG_DIR; ?>/omniInstruction.png" alt="iphone">
                </div>
                <div class="right">
                    <div class="service-2 wow fadeIn animated">
						<div class="icons">
                            <i class="material-icons primary">screen_share</i>
                        </div>
                        <div class="text">
                            <a href="service-item.html" class="primary-hover">Specialized Indicator</a>
                            <p>Threshold signal and BTS is shown</p>
                        </div>
                        
                    </div>
                    <div class="service-2 wow fadeIn animated">
                        <div class="icons">
                            <i class="material-icons primary">mic</i>
                        </div>
                        <div class="text">
                            <a href="service-item.html" class="primary-hover">Real time trading</a>
                            <p>Instant trade with Open + Call/Put, "Force Settle" trade in seconds</p>
                        </div>
                    </div>
                    <div class="service-2 wow fadeIn animated">
                        <div class="icons">
                            <i class="material-icons primary">play_circle_filled</i>
                        </div>
                        <div class="text">
                            <a href="service-item.html" class="primary-hover">Algo automation</a>
                            <p>Provided with intelligent robot to help you trade without manual operation.</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- /.service-section -->
<?php /*
<!-- Call To Action Section
================================================== -->
<section id="call-to" class="call-to-section">
    <div class="call-to-layer main-gradient"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="wow fadeIn animated">Like what You see? <span>Get Particle</span> Now!</h3>
                <p class="wow fadeIn animated">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec
                    odio. Praesent libero. Sed
                    cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum.
                    Praesent mauris.</p>
                <button class="btn btn-default wow fadeInRight hvr-sweep-to-right button-gradient animated">Get it Now</button>
            </div>
        </div>
    </div>
</section>
<!-- /.call-to-section -->

<!-- Skills Section
================================================== -->
<section id="skills" class="skills-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="skills-headline">
                    <h4 class="wow fadeIn animated">Let <span>Our Skills</span> speak for Us</h4>
                    <p class="wow fadeIn animated">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec
                        odio. Praesent libero. Sed
                        cursus ante dapibus diam.</p>
                </div>
            </div>
            <div class="col-md-12 progress-bar-wrapper">
                <div class="progress-bars">
                    <div class="bar-wrapper">
                        <div class="col-md-3 col-sm-3 col-xs-4 wow fadeIn animated">Adobe creative cloud</div>
                        <div class="col-md-8 col-sm-8 col-xs-7">
                            <div class="progressBar skill-bar-gradient wow animated first-bar"></div>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-1">
                            <span class="counter">75</span><span>%</span>
                        </div>
                    </div>
                    <div class="bar-wrapper">
                        <div class="col-md-3 col-sm-3 col-xs-4 wow fadeIn animated">WORDPRESS</div>
                        <div class="col-md-8 col-sm-8 col-xs-7">
                            <div class="progressBar skill-bar-gradient wow animated second-bar"></div>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-1">
                            <span class="counter">80</span><span>%</span>
                        </div>
                    </div>
                    <div class="bar-wrapper">
                        <div class="col-md-3 col-sm-3 col-xs-4 wow fadeIn animated">Laravel</div>
                        <div class="col-md-8 col-sm-8 col-xs-7">
                            <div class="progressBar skill-bar-gradient wow animated third-bar"></div>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-1">
                            <span class="counter">50</span><span>%</span>
                        </div>
                    </div>
                    <div class="bar-wrapper">
                        <div class="col-md-3 col-sm-3 col-xs-4 wow fadeIn animated">GRAPHIC DESIGN</div>
                        <div class="col-md-8 col-sm-8 col-xs-7">
                            <div class="progressBar skill-bar-gradient wow animated fourth-bar"></div>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-1">
                            <span class="counter">70</span><span>%</span>
                        </div>
                    </div>
                    <div class="bar-wrapper">
                        <div class="col-md-3 col-sm-3 col-xs-4 wow fadeIn animated">HTML / CSS / JAVASCRIPT</div>
                        <div class="col-md-8 col-sm-8 col-xs-7">
                            <div class="progressBar skill-bar-gradient wow animated fifth-bar"></div>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-1">
                            <span class="counter">59</span><span>%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.skills-section -->

*/ ?>

<!-- Contact Section
================================================== -->

<!-- /.contact-section -->

<?php include("page_footer.php"); ?>

<?php 
/*
<!-- Style Switcher -->
<div class="style-switcher" id="styler">
    <div class="color-chooser">
        <div class="style-heading">
            <p>Choose Your Colors</p>
        </div>
        <!-- /.style-heading -->

        <ul class="colors-list">
            <li>
                <div class="color-picker">
                    <p>Primary Color</p>
                    <div id="cp1" class="input-group colorpicker-component">
                        <input type="text" value="#9600f4" class="form-control" />
                        <span class="input-group-addon"><i></i></span>
                    </div>
                </div>
                <!-- /.color-picker -->
            </li>
            <li>
                <div class="color-picker">
                    <p>Primary color 2</p>
                    <div id="cp2" class="input-group colorpicker-component">
                        <input type="text" value="#7100B7" class="form-control" />
                        <span class="input-group-addon"><i></i></span>
                    </div>
                </div>
                <!-- /.color-picker -->
            </li>
            <li>
                <div class="color-picker">
                    <p>Primary Color 3</p>
                    <div id="cp3" class="input-group colorpicker-component">
                        <input type="text" value="#B649FF" class="form-control" />
                        <span class="input-group-addon"><i></i></span>
                    </div>
                </div>
                <!-- /.color-picker -->
            </li>
            <li>
                <div class="color-picker">
                    <p>Primary Color 4</p>
                    <div id="cp4" class="input-group colorpicker-component">
                        <input type="text" value="#D42BFF" class="form-control" />
                        <span class="input-group-addon"><i></i></span>
                    </div>
                </div>
                <!-- /.color-picker -->
            </li>
            <li>
                <div class="color-picker">
                    <p>Secondary color</p>
                    <div id="cp5" class="input-group colorpicker-component">
                        <input type="text" value="#00F2FF" class="form-control" />
                        <span class="input-group-addon"><i></i></span>
                    </div>
                </div>
                <!-- /.color-picker -->
            </li>
            <li>
                <div class="color-picker">
                    <p>Secondary color 2</p>
                    <div id="cp6" class="input-group colorpicker-component">
                        <input type="text" value="#00BBFF" class="form-control" />
                        <span class="input-group-addon"><i></i></span>
                    </div>
                </div>
                <!-- /.color-picker -->
            </li>
            <li>
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#style-modal">
                    Get CSS
                </button>
                <a href="index.html#" class="btn btn-default">Buy Now</a>
            </li>
        </ul>

    </div>
    <!-- /.styles -->
    <div class="gear" id="gear-click">
        <i class="fa fa-cog fa-spin fa-fw slow-spin"></i>
    </div>
    <!-- /.gear -->
</div>
<!-- /.style-switcher -->



<!-- Modal -->
<div class="modal fade" id="style-modal" tabindex="-1" role="dialog" aria-labelledby="style-modal-label">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="style-modal-label">Your Colors CSS</h4>
            </div>
            <div class="modal-body">
                <pre>
                    <code id="cssStyles">

                    </code>
                </pre>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="downloadLink">Download CSS</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
*/ ?>


<?php include("global_footer.php"); ?>
</body>
</html>


