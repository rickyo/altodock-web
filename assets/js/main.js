$(document).ready(function () {
    // Navigation
    var pages = ['demo-selection.html', 'index_dark.html', 'index_dark_video.html', 'index_dark_yt.html', 'index_light.html', 'index_light_video.html', 'index_light_yt.html'];
    var pageLink = $('.select');

    pageLink.on('click', function () {
        if ($.inArray($(this).attr('href'), pages) !== -1) {
            return true;
        } else {
           return false;
        }

    });

    // Responsive demo
    var resDemo = $('.responsive-demo');
    var iframe = $('#pageFrame');

    resDemo.on('click', function (e) {

        if (iframe.is('[class^="frame-"]')) {
            if (iframe.hasClass(this.id)) {
                iframe.removeClass();
            } else {
                iframe.removeClass();
                iframe.addClass(this.id);
            }
        } else {
            iframe.addClass(this.id);
        }

        $('body').css({'overflow': 'initial'});
        e.preventDefault();
    });

    $('#frame-desktop').on('click', function() {
        iframe.attr("height", $(window).outerHeight() + "px");
        $('.demo-index').css('overflow', 'hidden');
    });

    // Close the frame
    $('#close').on('click', function (e) {
        var currentUrl = window.location.pathname;
        var currentPage = pages[0];

        window.location.replace(currentUrl + currentPage);

        e.preventDefault();
    });

    iframe.attr("height", $(window).outerHeight() + "px");
});
